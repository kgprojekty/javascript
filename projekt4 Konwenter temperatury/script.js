const conv = document.querySelector('.conv');
const reset = document.querySelector('.reset');
const change = document.querySelector('.change');
const result = document.querySelector('.result');
const converter = document.querySelector('#converter');
const one = document.querySelector('.one');
const two = document.querySelector('.two');
const number = document.querySelector('number');

let one1 = '°C';
let two1 = '°F';
let converter1;

const conv1 = () => {
 if(converter.value !==''){       
  if(one.textContent === '°C') {
    converter1 = converter.value*1.8+32;
    result.textContent = `${converter.value}°C jest równe ${converter1.toFixed(1)}°F`;
    converter.value = '';
   } else {converter1 = (converter.value-32)/1.8;
         result.textContent = `${converter.value}°F jest równe ${converter1.toFixed(1)}°C`
         converter.value = '';
        }
  } else {result.textContent = 'Podaj temperaturę.'
         }        
}

const reset1 = () => {
 converter.value = reset;
 result.textContent = '';
}

const change1 = () => {
 if(one.textContent === '°C') {
   one.textContent = '°F';
   two.textContent = '°C';
 } else {
    one.textContent = '°C';
    two.textContent = '°F';
        }
}

conv.addEventListener('click', conv1);
reset.addEventListener('click', reset1);
change.addEventListener('click', change1);
