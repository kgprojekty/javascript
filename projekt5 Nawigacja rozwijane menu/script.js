const bars = document.querySelector('.fas fa-bars');
const times = document.querySelector('.fas fa-times hide');
const burger = document.querySelector('.burger');
const nav = document.querySelector('nav ul');

const handleNav = () => {
  nav.classList.toggle('active');
  burger.classList.toggle('active');
  bars.classList.toggle('hide');
  times.classList.toggle('hide');
}

burger.addEventListener('click', handleNav);

