const sizeUp = document.querySelector('.sizeUp');
const sizeDown = document.querySelector('.sizeDown');
const color = document.querySelector('.color');
const text = document.querySelector('.text');
const p = document.querySelector('p');

let fontSize = 36;

const sUp = () => {
    fontSize += 5;
    p.style.fontSize = fontSize + 'px';
}

const sDown = () => {
    if(fontSize <= 21) return;

    fontSize -= 5;
    p.style.fontSize = fontSize + 'px';
}

const colors = () => {
    const r = Math.floor(Math.random()*255);
    const g = Math.floor(Math.random()*255);
    const b = Math.floor(Math.random()*255);

    p.style.color = (`rgb(${r},${g},${b})`);
}

sizeUp.addEventListener('click', sUp);
sizeDown.addEventListener('click', sDown);
color.addEventListener('click', colors);