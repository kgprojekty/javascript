let todoInput; //miejsce, gdzie użytkownik wpisuje treść zadania
let errorInfo; //info o braku zadania lub konieczności wpisania tekstu
let addBtn; //przycisk ADD (dodanie nowych zadań do listy)
let ulList; //lista zadań, tagi ul
let newTodo //nowe zadanie (nowe li)

let popup; //info o błędzie
let popupInfo; //info w popup, jeśli doda się tyllko spację (pusty tekst)
let todoToEdit; //edytowany Todo
let popupInput; //input w popup
let popupAddBtn; //przycisk "zatwierdź" w popup
let popupCloseBtn; //przycisk "anuluj" w popup

const main = () => {
 prepareDOMElements();
 prepareDOMEvents();   //
}

const prepareDOMElements = () => {    //pobranie wszystkich elementów
 todoInput = document.querySelector('.todo-input');
 errorInfo = document.querySelector('.error-info');
 addBtn = document.querySelector('.btn-add');
 ulList = document.querySelector('.todolist ul');

 popup = document.querySelector('.popup');
 popupInfo = document.querySelector('.popup-info');
 popupInput = document.querySelector('.popup-input');
 popupAddBtn = document.querySelector('.accept');
 popupCloseBtn = document.querySelector('.cancel');
}

const prepareDOMEvents = () => {    //nadanie nasłuchiwania
 addBtn.addEventListener('click', addNewTodo);
 ulList.addEventListener('click', checkClick);
 popupCloseBtn.addEventListener('click', closePopup);
 popupAddBtn.addEventListener('click', changeTodoText);
 todoInput.addEventListener('keyup', enterKeyCheck)
}

const addNewTodo = () => {
 if(todoInput.value !=='') {
    newTodo = document.createElement('li');
    newTodo.textContent = todoInput.value;
    createToolsArea();

    ulList.append(newTodo);

    todoInput.value = '';
    errorInfo.textContent = '';
  } else {
    errorInfo.textContent = 'Wpisz treść zadania!';
    }
}

const createToolsArea = () => {
  const toolsPanel = document.createElement('div');
  toolsPanel.classList.add('tools');
  newTodo.append(toolsPanel);

  const completeBtn = document.createElement('button');
  completeBtn.classList.add('complete');
  completeBtn.innerHTML = '<i class="fas fa-check"></i>';

  const editBtn = document.createElement('button');
  editBtn.classList.add('edit');
  editBtn.textContent = 'EDIT';

  const deleteBtn= document.createElement('button');
  deleteBtn.classList.add('delete');
  deleteBtn.innerHTML = '<i class="fas fa-times"></i>';

  toolsPanel.append(completeBtn, editBtn, deleteBtn);
}

const checkClick = e => {
 if(e.target.matches('.complete')) {
   e.target.closest('li').classList.toggle('completed');
   e.target.classList.toggle('completed'); //wyszarzenie tika
 } else if(e.target.matches('.edit')){
    editTodo(e);
    } else if(e.target.matches('.delete')) {
      deleteTodo(e);
    }
}

const editTodo = e => {  //pobranie popup - zmienienie tła
  todoToEdit =  e.target.closest('li');

  popupInput.value = todoToEdit.firstChild.textContent;
  console.log(todoToEdit.firstChild);
  popup.style.display = 'flex';
}

const closePopup = () => {
  popup.style.display = 'none';
  popupInfo.textContent = '';
}

const changeTodoText = () => {    //edytowanie listy Todo
  if(popupInput.value !== '') {    //nie wpisana pusta wartość, np. spacja
  todoToEdit.firstChild.textContent = popupInput.value;
  popup.style.display = 'none';
  popupInfo.textContent = '';
  } else  {
     popupInfo.textContent = 'Podaj zadanie do wykonania';
   }
}

const deleteTodo = e => {   //usuwanie zadań z listy
  e.target.closest('li').remove();

  const allTodos = ulList.querySelectorAll('li');

  if(allTodos.length === 0) {         //jesli wszytskie zadania z listy są usunięte, pojawia się komunikat
   errorInfo.textContent = 'Brak zadań na liście.'
  }
}

const enterKeyCheck = e => {    //sprawdzenie, czy uzytkownik kliknął enter
  if(e.key === 'Enter') {
    addNewTodo();
  }
}

document.addEventListener('DOMContentLoaded', main);   //po załadowaniu strony
